json.extract! brand, :id, :name, :owner, :range, :crueltly, :created_at, :updated_at
json.url brand_url(brand, format: :json)
