class BrandsController < ApplicationController
  before_action :set_brand, only: [:show, :edit, :update, :destroy]

  # GET /brands
  # GET /brands.json
  def index
		if current_admin
				@brands = Brand.all
		elsif current_user
			role = current_user.role
			if role == "contributor"
				@brands = Brand.where(user_id:current_user.id)
		else    
			@brands = Brand.all
		end
     end
   end

def user
    @users = User.all
end

  # GET /brands/1
  # GET /brands/1.json
  def show
  end

  # GET /brands/new
  def new
  	role= current_user.role
      if role == "suscriber"
		      flash[:notice]= "You're not able to create a Brand"
		      redirect_to brand_path
      else    
    	    @brands = Brand.new
    	end
  end

  # GET /brands/1/edit
  def edit
  end

  # POST /brandss
  # POST /brands.json
  def create
    @brand = Brand.new(brand_params)
   	# @product= @brand.products.create(product_params)
	  @brand.user_id = current_user.id 

    respond_to do |format|
      if @brand.save
        format.html { redirect_to @brand, notice: 'Brand was successfully created.' }
        format.json { render :show, status: :created, location: @brand }
      else
        format.html { render :new }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /brands/1
  # PATCH/PUT /brands/1.json
  def update
    respond_to do |format|
      if @brand.update(brand_params)
        format.html { redirect_to @brand, notice: 'Brand was successfully updated.' }
        format.json { render :show, status: :ok, location: @brand }
      else
        format.html { render :edit }
        format.json { render json: @brand.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /brands/1
  # DELETE /brands/1.json
  def destroy
    @brand.destroy
    respond_to do |format|
      format.html { redirect_to brands_url, notice: 'Brand was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_brand
      @brand = Brand.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def brand_params
      params.require(:brand).permit(:name, :owner, :range, :crueltly, :avatar)
    end
end
