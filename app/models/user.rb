class User < ApplicationRecord
  has_many :brands

  class EmailValidator < ActiveModel::EachValidator
          def validate_each(record, attribute, value)
            unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
              record.errors[attribute] << (options[:message] || "It's not an email")
            end
          end
        end
        validates :email, email: true
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
end
