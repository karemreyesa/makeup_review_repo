class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
  mount_uploader :avatar, AvatarUploader
  is_impressionable 
  acts_as_votable
end
