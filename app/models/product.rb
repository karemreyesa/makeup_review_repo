class Product < ApplicationRecord
    belongs_to :brand

    mount_uploader :avatar, AvatarUploader
    def score
        self.get_upvotes.size - self.get_downvotes.size
    end

end
