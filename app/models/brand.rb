class Brand < ApplicationRecord
    has_many :products
    belongs_to :user
  
    
    validates :name, presence: true, length: { maximum: 30 }
    validates :owner, presence: true, length: { maximum: 30 }
    validates :range, presence: true, length: { maximum: 7 }
    validates :avatar, presence: true
    
    mount_uploader :avatar, AvatarUploader
    
    
#    class EmailValidator < ActiveModel::EachValidator
#      def validate_each(record, attribute, value)
#        unless value =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
#          record.errors[attribute] << (options[:message] || "No es un E-Mail")
#        end
#      end
#    end
#    validates :email, email: true
    
end


