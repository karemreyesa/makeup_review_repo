class CreateProducts < ActiveRecord::Migration[5.0]
  def change
    create_table :products do |t|
      t.string :name
      t.string :color
      t.float :price
      t.string :kind

      t.timestamps
    end
  end
end
