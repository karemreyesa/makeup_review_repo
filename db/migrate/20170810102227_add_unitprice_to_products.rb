class AddUnitpriceToProducts < ActiveRecord::Migration[5.0]
  def change
    add_column :products, :unitprice, :string
  end
end
