class AddAvatarToBrands < ActiveRecord::Migration[5.0]
  def change
    add_column :brands, :avatar, :string
  end
end
