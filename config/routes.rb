Rails.application.routes.draw do
 
  resources :links
  resources :products
  resources :brands
  devise_for :users, controllers:{
    sessions: 'users/sessions'
  }
   #nos dice donde estan las sessions
 devise_for :admins, controllers:{
    sessions: 'admins/sessions'
  }
  devise_scope :admin do
      get "admins/user/list",
      to: "admins/sessions#list",
      as: "list_user"
  end
  
  resources :brands do
  resources :products
      end

resources :products do
  member do
    put "like", to: "products#upvote"
    put "dislike", to: "products#downvote"
  end
end



 # get "users/list", to: "brands#user", as: "list_user"
    root to: 'brands#index'
 # get '/sexto/cerebros',to: 'products#index', as: 'products'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  

end
